const ArrayLike = require('./index.js');

describe('Test case for shift method', () => {
  test('Instance has not Own Property shift', () => {
    const arr = new ArrayLike();
    expect(arr.hasOwnProperty('shift')).toBeFalsy();
  })

  test('Instance has method shift', () => { 
    const arr = new ArrayLike();
    expect(arr.shift).toBeInstanceOf(Function);
  })

  test('The length of method equal 0', () => {
    expect(ArrayLike.prototype.shift).toHaveLength(0);
  })
 
  test('A method returns the removed element', () => { 
    const arr = new ArrayLike('ангел', 'клоун', 3, 'хирург');
    expect(arr.shift()).toBe('ангел');
    expect(arr.shift()).toBe('клоун');
    expect(arr.shift()).toBe(3);
  })
 
  test('Calling method on an empty array returns `undefined`', () => {
    const arr = new ArrayLike();
    expect(arr.shift()).toBeUndefined();
  })
  
  test('A method removes the first item of an array', () => {
    const arr = new ArrayLike(1,2,3);
    const firstEl = arr[0];
    const lastEl = arr[2];
    expect(arr.shift()).toEqual(firstEl);
    expect(arr.shift()).not.toEqual(lastEl);
 })

  test('A method method don`t take arguments', () => {  
    const arr = new ArrayLike();
    expect(arr.shift.length).toBe(0);
    expect(arr.shift.length).not.toBe(1);
  })

  test('A method decreases the length of the array by 1', () => { 
    const arr = new ArrayLike(1,2,3,4,5);
    expect(arr).toHaveLength(5);
    arr.shift();
    expect(arr).toHaveLength(4);
    arr.shift();
    expect(arr).not.toHaveLength(4);
  })

  test('An array element with index 1 after the shift must have an index', () => {  
    const arr = new ArrayLike(1,2,3,4,5);
    arr.shift();
    expect(arr[0]).toBe(2);
    arr.shift();
    expect(arr[0]).toBe(3);
  })
 
  test('Called correctly on other objects', () => {  
    const arr = new ArrayLike(1,2,3,4,5);
    expect(() => arr.shift.call({})).not.toThrow()
  })

  test('A method called on an empty object with no arguments added a property length with the value `0` to this object', () => { 
    const obj = {}; 
    const arr = new ArrayLike();
    arr.shift.call(obj);
    expect(obj).toHaveProperty('length');
    expect(obj).toHaveLength(0);
  })

  test('The number of elements in a non-empty array is reduced by one', () => {
    const arr = new ArrayLike(1,2,3,4,5);
    arr.shift();
    expect(arr.length).toBe(4);
  })
});