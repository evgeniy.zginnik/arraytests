const ArrayLike = require('./index.js');

describe('Test case for static method from', () => {
  test('Class has own method', () => {
    expect(ArrayLike).hasOwnProperty('from');
    expect(ArrayLike.from).toBeInstanceOf(Function);
  })

  test('The length of method equal 1', () => {
    expect(ArrayLike.from).toHaveLength(1);
  })

  test('Return a new instance of Array', () => {  
    const array = ArrayLike.from('javascript');
    expect(array).toBeInstanceOf(ArrayLike);
  })

  test('The length of a new array must be equal to length of a first argument which the method takes', () => {
    expect(ArrayLike.from('ABC')).toHaveLength(3);
    expect(ArrayLike.from('5')).not.toHaveLength(5);
  })

  test('A call the method on pseudoarray, return new array', () => { 
    const arr = ArrayLike.from({0: 'Value 1', 1: 'Value 2', length: 2});
    expect(arr[0]).toBe('Value 1');
    expect(arr[1]).toBe('Value 2');
  })

  test('Method with argument as number return an empty array', () => { // добавить обьект
    const arr = ArrayLike.from(1);
    expect(arr).toHaveLength(0);
    
    const arrSecond = ArrayLike.from({name:'Jack'});
    expect(arrSecond).toHaveLength(0);
  })

  describe('Test case with iterable argument', () => {
    test('Returned arr should contain all of argument\'s item ', () => {
      const array = ArrayLike.from('fob');
      expect(array[0]).toBe('f');
      expect(array[1]).toBe('o');
      expect(array[2]).toBe('b');
    })

    test('Returned new array with character-by-character elements', () => {
      const arr = ArrayLike.from('123   abc');
      expect(arr[0]).toBe('1');
      expect(arr[8]).toBe('c');
      const arrSecond = ArrayLike.from([1,2,3]);
      expect(arrSecond[0]).toBe(1);
      expect(arrSecond[2]).toBe(3);
    })
  })

  test('The second argument is called for each array\'s element', () => {
    const mockCallback = jest.fn(x => x + x);
    const arr = ArrayLike.from([1, 2, 3], mockCallback);
    expect(arr[0]).toBe(2);
    
    const mockCallbackSecond = jest.fn(x => x + x);
    const arrSecond = ArrayLike.from('123  67', mockCallbackSecond);
    expect(arrSecond[4]).toEqual('  '); 
    expect(mockCallbackSecond).toBeCalledTimes(7);
  })

  test(`The non-primitive third argument is used as 'this' inside callback`, () => {
    const obj = {
      data: 'abc'
    }; 
    const mockCallback = jest.fn(function () { 
      return this.data;
    });
    const arr = ArrayLike.from('H', mockCallback, obj);
    expect(arr[0]).toEqual('abc'); // 
  })

  describe('Test case with non valid argument', () => {
    test('A call method without arguments throw a TypeError', () => {
      expect(() => {
        ArrayLike.from();
      }).toThrow();
    })

    test('A call method with non callable callback throw a TypeError', () => { 
      expect(() => {
        ArrayLike.from(1, 2);
      }).toThrow(); 
    })
  })
})