# Tests cases for methods of array
Unit tests for JavaScript methods of array on Jest

## Summary
### Test Types
You can read about different types of tests in more depth here and here and here.
In general, the most important test types for a website are:
- **Unit Tests** Testing of individual units like functions or classes by supplying input and making sure the output is as expected:
- **Integration Tests** Testing processes across several units to achieve their goals, including their side effects:
- **End-to-end Tests** (also known as e2e tests or functional tests)- Testing how scenarios function on the product itself, by controlling the browser or the website. These tests usually ignore the internal structure of the application entirety and look at them from the eyes of the user like on a black box.

Approaches to development, when tests are written first, and then code.
DD (* something * Driven Development) - development based on something.

- TDD (Test Driven Development) - Test Driven Development.
- BDD (Behavior Driven Development) - Behavior Driven Development. BDD is actually an extension of the TDD approach. However, they are designed for different purposes and different tools are used to implement them. Different teams can interpret these concepts differently, and there is often confusion between them.

Additionally, we will be using a process called Test Driven Development, commonly called TDD, where we author tests to assert expected vs. actual results before authoring the code that produces those results. While TDD can be used on a variety of types of tests, we'll be applying the TDD workflow and process with unit tests.

## Tests cases for array methods
This repository contains examples of tests for the following array methods:
- constructor;
- symbol.iterator;
- from;
- unshift;
- shift;
- slice.


## To create the tests, we used the Javascript library - Jest
### Jest Tutorial
Jest is a delightful JavaScript Testing Framework with a focs on simplicity. 
Install Jest using `npm`

`npm i jest --save-dev`

Add the following section to your package.json:
```
"scripts": {
    "test": "jest"
  }
```

To run all tests 
`npm test`
